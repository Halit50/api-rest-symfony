<?php

namespace App\Controller;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthorController extends AbstractController
{
    // appeler tous les auteurs 
    #[Route('/api/authors', name: 'app_author_all', methods:['GET'])]
    public function getAllAuthor(AuthorRepository $authorRepository, SerializerInterface $serializer)
    {
       $authorList = $authorRepository->findAll();
       $authorJson = $serializer->serialize($authorList, 'json', ['groups' => 'getAuthors']);

       return new JsonResponse($authorJson, Response::HTTP_OK, [], true);
    }

    // Appeler un seul auteur avec son id
    #[Route('/api/author/{id}', name: 'app_author', methods:['GET'])]
    public function getAuthor($id, AuthorRepository $authorRepository, SerializerInterface $serializer)
    {
       $author = $authorRepository->find($id);
       $authorJson = $serializer->serialize($author, 'json', ['groups' => 'getAuthors']);

       return new JsonResponse($authorJson, Response::HTTP_OK, [], true);
    }

    // Supprimer un auteur avec son id
    #[Route('api/author/{id}', name: 'deletAuthor', methods:['DELETE'])]
    public function deleteAuthor(Author $author, EntityManagerInterface $em)
    {
        $em->remove($author);
        $em->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    //ajouter un auteur
    #[Route('api/author', name: 'createAuthor', methods:['POST'])]
    public function createAuthor(SerializerInterface $serialize, Request $request, EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator, ValidatorInterface $validator)
    {
        $author = $serialize->deserialize($request->getContent(), Author::class,'json');

        $errors = $validator->validate($author);

        if ($errors->count() > 0){
            return new JsonResponse($serialize->serialize($errors, 'json'),JsonResponse::HTTP_BAD_REQUEST,[],true);
        }

        $em->persist($author);
        $em->flush();

        $jsonAuthor = $serialize->serialize($author, 'json', ['groups' => 'getAuthors']);

        $location = $urlGenerator->generate('app_author', ['id' => $author->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonAuthor, Response::HTTP_CREATED,["Location" => $location], true );

    }

    //Modifier un auteur
    #[Route('api/author/{id}', name: 'updateAuthor', methods:['PUT'])]
    public function updateAuthor(SerializerInterface $serializer, Request $request, EntityManagerInterface $em, Author $currentAuthor)
    {
        $updateAuthor = $serializer->deserialize($request->getContent(), Author::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $currentAuthor]);

        $em->persist($updateAuthor);
        $em->flush();
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }


}
