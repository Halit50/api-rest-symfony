<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        //Création d'un user normal
        $user = new User();
        $user->setEmail('user@bookapi.com');
        $user->setRoles(['ROLE_USER']);
        $hashPassword = $this->hasher->hashPassword($user, 'password');
        $user->setPassword($hashPassword);
        $manager->persist($user);

        //Création d'un admin
        $userAdmin = new User();
        $userAdmin->setEmail('admin@bookapi.com');
        $userAdmin->setRoles(['ROLE_ADMIN']);
        $hashPassword = $this->hasher->hashPassword($userAdmin, 'password');
        $userAdmin->setPassword($hashPassword);
        $manager->persist($userAdmin);

        //Création des auteurs
        $listAuthors = [];
        for ($i = 0; $i < 10; $i++){
            $author = new Author();
            $author->setLastname('Nom' . $i);
            $author->setFirstname('Prénom' . $i);
            $manager->persist($author);
            // On sauvegarde l'auteur crée dans un tableau
            $listAuthors[]= $author;
        }

        //Création des livres
        for ($i = 0; $i < 20; $i++){
            $book = new Book();
            $book->setTitle('Titre'.$i);
            $book->setCoverText('Quatrieme de couverture numéro:'.$i);
            // On lie le livre à un auteur pris au hasard dans le tableau des auteurs
            $book->setAuthor($listAuthors[array_rand($listAuthors)]);
            $book->setComment("Commentaire du bibliothécaire". $i);
            $manager->persist($book);
        }

        $manager->flush();
    }
}
